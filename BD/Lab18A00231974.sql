--Diego Betanzos Esquer - A00231974
--Parte 1

SET DATEFORMAT dmy

select sum(e.cantidad) As "Total Cantidad 1997", sum(e.cantidad*m.costo*(1 + m.PorcentajeImpuesto/100)) As "Total Importe 1997"
from entregan e, materiales m
where m.clave = e.clave AND e.fecha between '01/01/1997' AND '31/12/1997'

select pr.razonsocial As "Raz�n Social", count(e.rfc) As "Cantidad de Entregas", sum(e.cantidad*m.costo*(1 + m.PorcentajeImpuesto/100)) As "Total Importe de Entregas"
from entregan e, materiales m, proveedores pr
where m.clave = e.clave AND pr.rfc = e.rfc
group by pr.razonsocial

select m.clave As Clave, m.descripcion As Descripci�n, sum(e.cantidad) As "Cantidad Total Entregada", max(e.cantidad) As "Cantidad M�xima Entregada", min(e.cantidad) As "Cantidad M�nima Entregada", sum(e.cantidad*m.costo*(1 + m.PorcentajeImpuesto/100)) As "Importe Total de Entregas"
from entregan e, materiales m
where e.clave = m.clave
group by m.clave, m.descripcion
having avg(e.cantidad) > 400

select pr.razonsocial As "Raz�n Social", avg(e.cantidad) As "Promedio de Cantidad de Entregas", m.clave As Clave, m.descripcion As Descripci�n
from proveedores pr, entregan e, materiales m
where pr.rfc = e.rfc AND e.clave = m.clave
group by pr.razonsocial, m.clave, m.descripcion
having avg(e.cantidad) >= 500

select pr.razonsocial As "Raz�n Social", avg(e.cantidad) As "Promedio de Cantidad de Entregas", m.clave As Clave, m.descripcion As Descripci�n
from proveedores pr, entregan e, materiales m
where pr.rfc = e.rfc AND e.clave = m.clave
group by pr.razonsocial, m.clave, m.descripcion
having avg(e.cantidad) < 370 OR avg(e.cantidad) > 450

--Parte 2

INSERT INTO Materiales VALUES (1440, 'Block com�n', 224, 2.88)
INSERT INTO Materiales VALUES (1450, 'Block doble altura', 400, 2.9)
INSERT INTO Materiales VALUES (1460, 'Block doble ancho', 395, 2.92)
INSERT INTO Materiales VALUES (1470, 'Ladrillo', 110, 2.94)
INSERT INTO Materiales VALUES (1480, 'Ladrillo doble', 196, 2.95)

select * from Materiales

select clave As Clave, descripcion As Descripci�n from materiales
where clave NOT IN (select clave from entregan)

select razonsocial As "Raz�n Social" from proveedores
where rfc IN (select e.rfc from entregan e, proyectos p
			  where e.numero = p.numero AND (p.denominacion LIKE 'Vamos Mexico'
										  OR p.denominacion LIKE 'Queretaro Limpio'))

select descripcion As Descripci�n from materiales
where clave NOT IN (select e.clave from entregan e, proyectos p
				where e.numero = p.numero AND p.denominacion LIKE 'CIT Yucatan')

select pr.razonsocial As "Raz�n Social", avg(e.cantidad) As "Promedio Cantidad" from proveedores pr, entregan e
where pr.rfc = e.rfc
GROUP BY pr.razonsocial
HAVING avg(e.cantidad) > (select avg(cantidad) from entregan where rfc LIKE 'VAGO780901' GROUP BY rfc)

--La creacion de las siguientes 2 vistas tiene que ser en ventanas aparte
create view cantidad2000(RFC, Cantidad) As (
	select rfc, sum(cantidad) from entregan where fecha between '01/01/2000' and '31/12/2000' group by rfc
)

create view cantidad2001(RFC, Cantidad) As (
	select rfc, sum(cantidad) from entregan where fecha between '01/01/2001' and '31/12/2001' group by rfc
)

select e.rfc As RFC, pr.razonsocial As "Raz�n Social" from entregan e, proveedores pr, proyectos p, cantidad2000 c2000, cantidad2001 c2001
where pr.rfc = e.rfc and p.numero = e.numero and e.rfc = c2000.rfc and e.rfc = c2001.rfc and p.denominacion like 'Infonavit Durango' and c2000.rfc = c2001.rfc and c2000.cantidad > c2001.cantidad
group by e.rfc, pr.razonsocial

--Verificando la ultima jaja
select pr.razonsocial As RazonSocial2000, sum(e.cantidad) As Cantidad from proveedores pr, entregan e
where pr.rfc = e.rfc and e.fecha between '01/01/2000' and '31/12/2000'
group by pr.razonsocial
order by Cantidad

select pr.razonsocial As RazonSocial2001, sum(e.cantidad) As Cantidad from proveedores pr, entregan e
where pr.rfc = e.rfc and e.fecha between '01/01/2001' and '31/12/2001'
group by pr.razonsocial
order by Cantidad

select distinct pr.razonsocial As RazonSocialDurango from proveedores pr, entregan e, proyectos p
where e.rfc = pr.rfc and e.numero = p.numero and p.denominacion like 'Infonavit Durango'

select * from Cantidad2000

select * from Cantidad2001