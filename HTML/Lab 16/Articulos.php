<?php
    session_start();
    require_once("util.php");
    include("_header.html");
    include("_agregar.html");
    echo getArticulos();
    include("_preguntas.html");
    include("_footer.html");
    if (isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        include("_mensaje.html");
        unset($_SESSION["mensaje"]);
    }
?>