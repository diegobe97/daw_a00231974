$(document).ready(function () {
    $(".button-collapse").sideNav();
    $('.slider').slider({
        indicators: false,
        interval: 10000
    });
    $(".dropdown-button").dropdown({
        hover: true
    });
    $('.modal').modal();

    $('.navbar2').pushpin({
        top: 27,
        offset: 0
    });
    
    $('.parallax').parallax();
});
