IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'creaMaterial' AND type = 'P')
	DROP PROCEDURE creaMaterial
GO
            
CREATE PROCEDURE creaMaterial
	@uclave NUMERIC(5,0),
	@udescripcion VARCHAR(50),
	@ucosto NUMERIC(8,2),
	@uimpuesto NUMERIC(6,2)
AS
	INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
GO

EXECUTE creaMaterial 5000,'Martillos Acme',250,15 

select * from materiales

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'modificaMaterial' AND type = 'P')
	DROP PROCEDURE modificaMaterial
GO
            
CREATE PROCEDURE modificaMaterial
	@uclave NUMERIC(5,0),
	@udescripcion VARCHAR(50),
	@ucosto NUMERIC(8,2),
	@uimpuesto NUMERIC(6,2)
AS
	UPDATE Materiales SET descripcion = @udescripcion, costo = @ucosto, PorcentajeImpuesto = @uimpuesto WHERE clave = @uclave
GO

EXECUTE modificaMaterial 5000,'Martillos Acmeee',280,16

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'eliminaMaterial' AND type = 'P')
	DROP PROCEDURE eliminaMaterial
GO
            
CREATE PROCEDURE eliminaMaterial
	@uclave NUMERIC(5,0)
AS
	DELETE FROM Materiales WHERE clave = @uclave
GO

EXECUTE eliminaMaterial 5001





IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'creaEntregan' AND type = 'P')
	DROP PROCEDURE creaEntregan
GO
            
CREATE PROCEDURE creaEntregan
	@uclave NUMERIC(5,0),
	@urfc VARCHAR(13),
	@unumero NUMERIC(5,0),
	@ufecha DATETIME,
	@ucantidad NUMERIC(8,2)
AS
	INSERT INTO Entregan VALUES(@uclave, @urfc, @unumero, @ufecha, @ucantidad)
GO

SET DATEFORMAT dmy
EXECUTE creaEntregan 5000,'ZZZZ0808423',4000,'18/10/2017',5000 

select * from Entregan

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'modificaEntregan' AND type = 'P')
	DROP PROCEDURE modificaEntregan
GO
            
CREATE PROCEDURE modificaEntregan
	@uclave NUMERIC(5,0),
	@urfc VARCHAR(13),
	@unumero NUMERIC(5,0),
	@ufecha DATETIME,
	@ucantidad NUMERIC(8,2)
AS
	UPDATE Entregan SET rfc = @urfc, numero = @unumero, fecha = @ufecha, cantidad = @ucantidad WHERE clave = @uclave and rfc = @urfc and numero = @unumero
GO

set dateformat dmy
EXECUTE modificaEntregan 5000,'ZZZZ0808423',5000,'18/10/2017',5200 

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'eliminaEntregan' AND type = 'P')
	DROP PROCEDURE eliminaEntregan
GO
            
CREATE PROCEDURE eliminaEntregan
	@uclave NUMERIC(5,0)
AS
	DELETE FROM Entregan WHERE clave = @uclave
GO

EXECUTE eliminaEntregan 5000





IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'creaProveedor' AND type = 'P')
	DROP PROCEDURE creaProveedor
GO
            
CREATE PROCEDURE creaProveedor
	@urfc VARCHAR(13),
	@urazonsocial VARCHAR(50)
AS
	INSERT INTO Proveedores VALUES(@urfc, @urazonsocial)
GO

EXECUTE creaProveedor 'ZZZZ0808423','ConstructoraBetanzos' 

select * from Proveedores

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'modificaProveedor' AND type = 'P')
	DROP PROCEDURE modificaProveedor
GO
            
CREATE PROCEDURE modificaProveedor
	@urfc VARCHAR(13),
	@urazonsocial VARCHAR(50)
AS
	UPDATE Proveedores SET rfc = @urfc, razonsocial = @urazonsocial WHERE rfc = @urfc
GO

EXECUTE modificaProveedor 'ZZZZ0808423','ConstructoraBetanzosEsquer' 

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'eliminaProveedor' AND type = 'P')
	DROP PROCEDURE eliminaProveedor
GO
            
CREATE PROCEDURE eliminaProveedor
	@urfc VARCHAR(13)
AS
	DELETE FROM Proveedores WHERE rfc = @urfc
GO

EXECUTE eliminaProveedor 'ZZZZ0808423'





IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'creaProyecto' AND type = 'P')
	DROP PROCEDURE creaProyecto
GO
            
CREATE PROCEDURE creaProyecto
	@unumero NUMERIC(5,0),
	@udenominacion VARCHAR(50)
AS
	INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
GO

EXECUTE creaProyecto 4000,'Pepsi Navojoa' 

select * from Proyectos

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'modificaProyecto' AND type = 'P')
	DROP PROCEDURE modificaProyecto
GO
            
CREATE PROCEDURE modificaProyecto
	@unumero NUMERIC(5,0),
	@udenominacion VARCHAR(50)
AS
	UPDATE Proyectos SET numero = @unumero, denominacion = @udenominacion WHERE numero = @unumero
GO

EXECUTE modificaProyecto 4000,'Pepsi Navojoa Sonora' 

IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'eliminaProyecto' AND type = 'P')
	DROP PROCEDURE eliminaProyecto
GO
            
CREATE PROCEDURE eliminaProyecto
	@unumero NUMERIC(5,0)
AS
	DELETE FROM Proyectos WHERE numero = @unumero
GO

EXECUTE eliminaProyecto '4000'





IF EXISTS (SELECT name FROM sysobjects 
	WHERE name = 'queryMaterial' AND type = 'P')
	DROP PROCEDURE queryMaterial
GO

CREATE PROCEDURE queryMaterial
	@udescripcion VARCHAR(50),
	@ucosto NUMERIC(8,2)
AS
	SELECT * FROM Materiales WHERE descripcion 
	LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
GO

EXECUTE queryMaterial 'Lad',20