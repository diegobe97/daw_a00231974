<?php
    session_start();
    require_once("util.php");

    if(isset($_POST["titulo"]) && isset($_POST["descripcion"]) && isset($_POST["categoria"]) && isset($_POST["url"])){
        $titulo = htmlspecialchars($_POST["titulo"]);
        $descripcion = htmlspecialchars($_POST["descripcion"]);
        $categoria = htmlspecialchars($_POST["categoria"]);
        $url = htmlspecialchars($_POST["url"]);

        if(isset($_POST["id"])){
            $id = htmlspecialchars($_POST["id"]);
            editarArticulo($id, $titulo, $descripcion, $categoria, $url);
            $_SESSION["mensaje"] = 'El artículo "'.$titulo.'" fue actualizado exitosamente.';
        }else{
            guardarArticulo($titulo, $descripcion, $categoria, $url);
            $_SESSION["mensaje"] = 'El artículo "'.$titulo.'" fue creado exitosamente.';
        }
        header("location:Articulos.php");
    }else{
        $_SESSION["mensaje"] = 'El artículo no se pudo crear.';
        header("location:Articulos.php");
    }
?>