<?php
    session_start();
    require_once("util.php");
    
    eliminarArticulo($_GET["id"]);
    
    $_SESSION["mensaje"] = 'El artículo fue eliminado exitosamente.';

    header("location:Articulos.php");
?>