-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2017 at 07:52 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `articulo`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `crearArticulo` (IN `titulo` VARCHAR(50), IN `descripcion` VARCHAR(250), IN `categoria` VARCHAR(50), IN `URL` VARCHAR(250))  begin
	INSERT INTO Articulo (`Titulo`, `Descripcion`, `Categoria`, `URL`) VALUES (titulo,descripcion,categoria,url);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `articulo`
--

CREATE TABLE `articulo` (
  `id` int(11) NOT NULL,
  `Titulo` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Descripcion` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Categoria` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `URL` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articulo`
--

INSERT INTO `articulo` (`id`, `Titulo`, `Descripcion`, `Categoria`, `URL`) VALUES
(2, 'Seguridad en Eventos Masivos', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque aliquam purus ac eros tincidunt bibendum. Quisque eu facilisis nunc, vitae sollicitudin magna. Vivamus interdum nec nibh non sagittis. Pellentesque in dignissim risus, nec egestas ipsum. Donec luctus ac velit eget faucibus.', 'Seguridad', 'http://www.fiso-web.org/Content/files/articulos-profesionales/4425.pdf'),
(3, 'safdghnbfvdsa', 'b ngrhagsvdcx hnrtsbd vxcv', 'terhtewsg', 'eqrth423qgrebgr425er'),
(4, 'Como comer mientras te proteges civilmente.', 'La comida es algo preocupante para los seres humanos porque sin ella se mueren, por esto te venimos a explicar como protegerte civilmente ante cualquier desastre natural que tu alimento pueda generar.', 'Proteccion Civil', 'www.alimentoprotegido.com'),
(5, 'Tecnologico de Monterrey', 'El ITESM te ayuda con tus proyectos, dinos que necesitas y ponemos a nuestros alumnos a chambear por ti. Saludoooossss!!!', 'Servicio Social', 'www.ssc.itesm.qro.emprendimiento.factortec.competitivosinternacionalmente.com'),
(6, 'Esta va de relleno.', 'asdvcnterqwfasdvz cerwav ceqrbf vevqrvaf cqe fv', 'uhqbevfljieqrbkw', 'www.kjsahfk-kuqgwjhb.com.mx'),
(7, 'Proteccion en la cultura', 'A no, era cultura de la proteccion', 'Culturaaaaaa', 'www.perdonporlabasuraderegistros.com'),
(8, 'Hola', 'Salu2 cali2 a to2', 'Saluditos', 'www.saludar.com'),
(9, 'zvfghdfa', 'rgsdhbfvr', 'rqewgvf', 'www.asdvar.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articulo`
--
ALTER TABLE `articulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
