$(document).ready(function () {
    $(".button-collapse").sideNav();
    $('.slider').slider({
        indicators: false,
        interval: 10000
    });
    $(".dropdown-button").dropdown({
        hover: true
    });
    $('.modal').modal();

    $('.navbar2').pushpin({
        top: 27,
        offset: 0
    });

    $('.parallax').parallax();
});


$(document).ready(function () {
    var trig = 1;
    $('#search-icon-inside').addClass('hide');
    $('#close-icon-inside').addClass('hide');
    $('#animacion').addClass('hide');
    
    //fix for chrome
    $("#search").addClass('searchbarfix');
    //animate searchbar width increase to  +150%
    $('#search').click(function (e) {
        //handle other nav elements visibility here to avoid push down 
        $('.MenuHeader').addClass('hide');
        $('#search-icon-inside').removeClass('hide');
        $('#close-icon-inside').removeClass('hide');
        if (trig == 1) {
            $('#navfix2').animate({
                width: '+=200',
                'left': '-250px'
            }, 400);
            trig++;
        }

    });

    // if user leaves the form the width will go back to original state

    $("#search").focusout(function () {
        $('#navfix2').animate({
            'left': '0px',
            width: '-=200'
        }, 400);
        trig = trig - 1;
        //handle other nav elements visibility first to avoid push down
        $('.MenuHeader').removeClass('hide');
        $('#search-icon-inside').addClass('hide');
        $('#close-icon-inside').addClass('hide');
    });
    
    $('#boton-animacion').click(function(){
        $('#animacion').removeClass('hide');
        var div = $('#animacion');
        div.animate({height: '500px', opacity: '0.4'}, "slow");
        div.animate({width: '500px', opacity: '0.8'}, "slow");
        div.animate({height: '50px', opacity: '0.4'}, "slow");
        div.animate({width: '50px', opacity: '0.8'}, "slow");
    });
});

function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } else {
    // Non AJAX browsers
    return(null);
  }
}


function sendRequest(){

   let JQUERY = 1;
   
   if(JQUERY == 1) {
       
        $.get("ssajax.php", { pattern: document.getElementById('categoria').value })
            .done(function( data ) {
                var ajaxResponse = document.getElementById('ajaxResponse');
                ajaxResponse.innerHTML = data;
                ajaxResponse.style.visibility = "visible";
         });
       
   } else {
       
       request=getRequestObject();
       if(request!=null)
       {
         var userInput = document.getElementById('categoria');
         var url='ssajax.php?pattern='+userInput.value;
         request.open('GET',url,true);
         request.onreadystatechange = 
                function() { 
                    if((request.readyState==4)){
                        // Asynchronous response has arrived
                        var ajaxResponse=document.getElementById('ajaxResponse');
                        ajaxResponse.innerHTML=request.responseText;
                        ajaxResponse.style.visibility="visible";
                    }     
                };
         request.send(null);
       }
   }
}


function selectValue() {
   var list=document.getElementById("list");
   var userInput=document.getElementById("categoria");
   var ajaxResponse=document.getElementById('ajaxResponse');
   userInput.value=list.options[list.selectedIndex].text;
   ajaxResponse.style.visibility="hidden";
   userInput.focus();
}
