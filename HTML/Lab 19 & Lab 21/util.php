<?php
    //error_reporting(E_ERROR | E_WARNING | E_PARSE);

    function conectar() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Articulo";

        $con = mysqli_connect($servername, $username, $password, $dbname);

        // Check Connection
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }

        return $con;
    }

    function desconectar($mysql) {
        mysqli_close($mysql);
    }

    function getArticulos() {
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM Articulo';
        // Query execution; returns identifier of the result group
        $articulos = $db->query($query);

        $cards = "";
        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($articulos, MYSQLI_BOTH)) {
            $cards .= '
            <br>
            <div class="col s12 m7">
                <h2 class="header">'.$fila["Titulo"].'</h2>
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="https://lorempixel.com/100/190/nature/6">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                        <p>'.$fila["Categoria"].'</p>
                        <p>'.$fila["Descripcion"].'</p>
                    </div>
                    <div class="card-action">
                      <a href="#">'.$fila["URL"].'</a>
                      <br>
                      <a href="editar.php?id='.$fila["id"].'">Editar</a>
                      <a href="eliminar.php?id='.$fila["id"].'">Eliminar</a>
                    </div>
                  </div>
                </div>
              </div>';

        }
        $cards .= '<br><br>';
        // it releases the associated results
        mysqli_free_result($articulos);

        desconectar($db);

        return $cards;
    }

    function guardarArticulo($titulo, $descripcion, $categoria, $url){
        $db = conectar();
        
        $storedProcedure = 1;
        
        if(strlen($titulo) >= 1 && strlen($titulo) <= 50 && strlen($descripcion) >= 1 && strlen($descripcion) <= 1000 && strlen($categoria) >= 1 && strlen($categoria) <= 50 && strlen($url) >= 1 && strlen($url) <= 250){
            // insert command specification 
            if($storedProcedure == 1){
                $query = 'CALL `crearArticulo`(?,?,?,?);';
            }else{
                $query='INSERT INTO Articulo (`Titulo`, `Descripcion`, `Categoria`, `URL`) VALUES (?,?,?,?)';
            }
            
            // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("ssss", $titulo, $descripcion, $categoria, $url)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
            // Executing the statement
            if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
            }
        }else{
            echo "Error en la longitud de los valores insertados en los campos.";
        }
        
        desconectar($db);
        //EXECUTE creaMaterial 5000,'Martillos Acme',250,15 
    }

    function editarArticulo($id, $titulo, $descripcion, $categoria, $url){
        $db = conectar();
        
        if(strlen($titulo) >= 1 && strlen($titulo) <= 50 && strlen($descripcion) >= 1 && strlen($descripcion) <= 1000 && strlen($categoria) >= 1 && strlen($categoria) <= 50 && strlen($url) >= 1 && strlen($url) <= 250){
            // insert command specification 
            $query='UPDATE Articulo SET Titulo=?, Descripcion=?, Categoria=?, URL=? WHERE id=?';
            // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("sssss", $titulo, $descripcion, $categoria, $url, $id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
            // Executing the statement
            if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
            }
        }else{
            echo "Error en la longitud de los valores insertados en los campos.";
        }
        
        desconectar($db);
    }

    function getArticulo($db, $id){
        //Specification of the SQL query
        $query='SELECT * FROM Articulo WHERE id = "'.$id.'"';
         // Query execution; returns identifier of the result group
        $art = $db->query($query);   
        $fila = mysqli_fetch_array($art, MYSQLI_BOTH);
        return $fila;
    }

    function eliminarArticulo($id){
        $db = conectar();
        
        // insert command specification 
        $query='DELETE FROM Articulo WHERE id = ?';
        
        // Preparing the statement 
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("s", $id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        desconectar($db);
    }

    function getCategoria() {
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM Articulo';
         // Query execution; returns identifier of the result group
        $result = $db->query($query);

        $ubicacion = array();

        while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $ubicacion[] = $fila['Categoria'];
        }
        // it releases the associated results
        mysqli_free_result($result);

        return $ubicacion;
    }
?>
