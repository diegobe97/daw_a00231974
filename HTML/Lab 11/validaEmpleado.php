<?php
    $nombre = htmlspecialchars($_POST["nombre"]);
    $apeP = htmlspecialchars($_POST["apeP"]);
    $apeM = htmlspecialchars($_POST["apeM"]);
    $puesto = htmlspecialchars($_POST["puesto"]);
    $usuario = htmlspecialchars($_POST["usuario"]);
    $pass = htmlspecialchars($_POST["pass"]);
    $passConfirm = htmlspecialchars($_POST["passConfirm"]);

    if(isset($nombre) && isset($apeP) && isset($puesto) && isset($usuario) && isset($pass) && isset($passConfirm) && ($pass == $passConfirm)) {
        include("_header.html");
        include("empleadoCreado.html");
        include("_footer.html");    
    } else {
        if(pass == passConfirm){
            $validPass = "Sí.";
        }else{
            $validPass = "No.";
        }
        include("_header.html");
        include("empleadoNoCreado.html");
        include("_footer.html");
    }
?>
