<?php include('_header.html'); ?>
<div class="container">
    <div class="row">
        <div class="col s12">
            <h5>Empleados</h5>
            <div class="divider"></div>
        </div>
    </div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <!-- Activador del Modal -->
    <a class="btn-flat-large modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Añadir empleado" href="#modal1"><i class="medium material-icons hoverable">add</i></a>
    <!-- Contenido del Modal -->
    <div id="modal1" class="modal modal-fixed-footer">
        <form id="formularioEmpleado" class="col s12" action="validaEmpleado.php" method="POST">
            <div class="modal-content">
                <h4>Añadir Empleado</h4>
                <div class="row">
                    <div class="row">
                        <div class="input-field col s6">
                            <input name="nombre" id="nombre" type="text" class="validate" minlength="1" maxlength="50" required>
                            <label for="nombre">Nombre</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="apeP" id="apeP" type="text" class="validate" minlength="1" maxlength="50" required>
                            <label for="apeP">Apellido Paterno</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input name="puesto" id="puesto" type="text" class="validate" minlength="1" maxlength="50" required>
                            <label for="puesto">Puesto</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="apeM" id="apeM" type="text" class="validate" minlength="0" maxlength="50">
                            <label for="apeM">Apellido Materno</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input name="usuario" id="usuario" type="number" class="validate" min="1000" max="9999" required>
                            <label for="usuario">Nombre Usuario</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input name="pass" id="pass" type="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}" minlength="7" maxlength="20" required>
                            <label for="pass">Contraseña</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="passConfirm" id="passConfirm" type="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}" minlength="7" maxlength="20" required>
                            <label for="passConfirm">Confirmar Contraseña</label>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
                <input type="submit" value="Aceptar" class="modal-action modal-close waves-effect waves-green btn-flat">
            </div>
        </form>
    </div>
</div>
<?php include('_footer.html'); ?>
