-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2017 at 01:48 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mascotas`
--

-- --------------------------------------------------------

--
-- Table structure for table `raza`
--

CREATE TABLE `raza` (
  `id` int(11) NOT NULL,
  `tipo` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `raza`
--

INSERT INTO `raza` (`id`, `tipo`) VALUES
(1, 'cocker spaniel'),
(2, 'pug'),
(3, 'beagle'),
(4, 'golden retriever'),
(5, 'pastor alemán'),
(6, 'chihuahua'),
(7, 'Chow Chow'),
(8, 'Mastín inglés');

-- --------------------------------------------------------

--
-- Table structure for table `registro`
--

CREATE TABLE `registro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `raza_id` int(11) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registro`
--

INSERT INTO `registro` (`id`, `nombre`, `raza_id`, `ubicacion`, `created_at`) VALUES
(1, 'Punky', 1, 'San Cristóbal de Las Casas, Chiapas', '2017-10-06 19:37:28'),
(2, 'Oc', 4, 'Jurica', '2017-10-06 19:43:05'),
(3, 'Muñe', 4, 'Ocasus', '2017-09-28 00:00:00'),
(4, 'Icker', 8, 'CDMX', '2017-10-06 00:00:00'),
(5, 'Schubert', 3, 'Querétaro Centro Sur', '0000-00-00 00:00:00'),
(6, 'Alan', 2, '4309', '2017-10-06 19:37:49'),
(7, 'Max', 7, 'Juriquilla', '2017-10-06 19:13:16'),
(8, 'Bobby', 3, 'Jurica', '2017-10-06 19:42:35'),
(9, 'Gary', 8, 'Bikini Bottom', '2017-10-06 19:43:39'),
(15, 'Me siento mallll', 4, 'Everywhere', '2017-10-12 03:39:13'),
(16, 'Boni', 5, 'Navojoa', '2017-10-12 03:39:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `raza`
--
ALTER TABLE `raza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raza_id` (`raza_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `raza`
--
ALTER TABLE `raza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `registro`
--
ALTER TABLE `registro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `registro_ibfk_1` FOREIGN KEY (`raza_id`) REFERENCES `raza` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
