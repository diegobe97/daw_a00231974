<?php
    session_start();

    $nombre = htmlspecialchars($_POST["nombre"]);
    $apeP = htmlspecialchars($_POST["apeP"]);
    $apeM = htmlspecialchars($_POST["apeM"]);
    $puesto = htmlspecialchars($_POST["puesto"]);
    $usuario = htmlspecialchars($_POST["usuario"]);
    $pass = htmlspecialchars($_POST["pass"]);
    $passConfirm = htmlspecialchars($_POST["passConfirm"]);

    if (isset($_SESSION["user"])) {
        include("_header.html");
        include("empleadoCreado.html");
        include("_footer.html");
    }else if(isset($nombre) && isset($apeP) && isset($puesto) && isset($usuario) && isset($pass) && isset($passConfirm) && ($pass == $passConfirm)) {
        
        $_SESSION["usuario"] = $usuario;
        $_SESSION["nombre"] = $nombre;
        $_SESSION["apeP"] = $apeP;
        $_SESSION["apeM"] = $apeM;
        $_SESSION["puesto"] = $puesto;
        $_SESSION["pass"] = $pass;
        
        $target_dir = "images/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["foto"]["tmp_name"]);
            if($check !== false) {
                echo "El archivo es una imagen - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "El archivo no es una imagen.";
                $uploadOk = 0;
            }
        }
        
        if (file_exists($target_file)) {
            echo "Lo sentimos, el archivo ya existe.";
            $uploadOk = 0;
        }
        
        if ($_FILES["foto"]["size"] > 500000) {
            echo "Lo sentimos, su belleza es demasiado grande para nosotros.";
            $uploadOk = 0;
        }
        
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Lo sentimos, solo aceptamos archivos en formato JPG, JPEG, PNG y GIF.";
            $uploadOk = 0;
        }
        
        if ($uploadOk == 0) {
            echo "Lo sentimos, su archivo no fue cargado.";
        
        } else {
            if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
                echo "El archivo ". basename( $_FILES["foto"]["name"]). " ha sido cargado exitosamente, WUJU!.";
            } else {
                echo "Lo sentimos, hubo un error al subir su archivo";
            }
        }
        
        $_SESSION["foto"] = $target_file;
        
        include("_header.html");
        include("empleadoCreado.html");
        include("_footer.html");    
    } else {
        if(pass == passConfirm){
            $validPass = "Sí.";
        }else{
            $validPass = "No.";
        }
        include("_header.html");
        include("empleadoNoCreado.html");
        include("_footer.html");
    }
?>
