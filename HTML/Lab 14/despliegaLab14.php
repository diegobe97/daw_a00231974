<?php
    require_once "util.php";

    $result = getUsuarios();
    
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_assoc($result)){
            echo "<tr>";
            echo "<td>".$row["NombreUsuario"]." </td>";
            echo "<td>".$row["Nombre"]." </td>";
            echo "<td>".$row["ApellidoPaterno"]." </td>";
            echo "<td>".$row["ApellidoMaterno"]." </td>";
            echo "<td>".$row["Puesto"]." </td>";
            echo "</tr>";
            echo "<br>";
        }
    }else{
        echo "<p>No hay usuarios en la base de datos</p>";
    }

    $IdEmpleado = 1234;

    $result = getNombreUsuarioByIdEmpleado($IdEmpleado);
    
    if(mysqli_num_rows($result) > 0){
        $row = mysqli_fetch_assoc($result);
        echo "<tr>";
        echo "<td>Nombre: ".$row["Nombre"]." </td>";
        echo "<td>IdEmpleado: ".$IdEmpleado." </td>";
        echo "</tr>";
        echo "<br>";
    }else{
        echo "<p>No se encontro un usuario con ese Id de Empleado.</p>";
    }

    $Puesto = "Jefa de Jefas";

    $result = getUsuariosByPuesto($Puesto);
    
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_assoc($result)){
            echo "<tr>";
            echo "<td>Nombre: ".$row["Nombre"]." </td>";
            echo "</tr>";
            echo "<br>";
        }
    }else{
        echo "<p>No se encontro un usuario con ese Id de Empleado.</p>";
    }

    echo "<br>";
    echo "<tr>";
    echo "<td>¿Qué es ODBC y para qué es útil?</td>";
    echo "<br>";
    echo "<td>Es un estándar de acceso a bases de datos. Es útil porque hace posible el acceder a cualquier dato desde cualquier aplicación, sin importar qué sistema de gestión de bases de datos almacene los datos.</td>";
    echo "<br>";
    echo "<br>";
    echo "<td>¿Qué es SQL Injection?</td>";
    echo "<br>";
    echo "<td>Es un tipo de ataque a una base de datos en el cual, por la mala filtración de las variables se puede inyectar un código creado por el atacante al propio código fuente de la base de datos.</td>";
    echo "<br>";
    echo "<br>";
    echo "<td>¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?</td>";
    echo "<br>";
    echo "<td>Escapar los caracteres especiales utilizados en las consultas SQL con funciones como mysql_real_scape_string() en PHP. Delimitar los valores de las consultas. Verificar siempre los datos que introduce el usuario con funciones como ctype_digit() para saber si es un número o ctype_alpha () para saber si se trata de una cadena de texto en PHP. Asignar mínimos privilegios al usuario que conectará con la base de datos. Y, finalmente, asegurarnos de que no hemos dejado ningún tipo de puertas abiertas, aunque suelen ser procesos caros realizados por terceras empresas.</td>";
    echo "</tr>";
?>