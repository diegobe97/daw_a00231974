<?php 
    function conectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Usuario";
        
        $con = mysqli_connect($servername, $username, $password, $dbname);
        
        // Check Connection
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        
        return $con;
    }

    function closeDb($mysql){
        mysqli_close($mysql);
    }

    function getUsuarios(){
        $conn = conectDb();
        
        $sql = "SELECT * FROM Usuario";
        
        $result = mysqli_query($conn, $sql);
        
        closeDb($conn);
        
        return $result;
    }

    function getNombreUsuarioByIdEmpleado($IdEmpleado){
        $conn = conectDb();
        
        $sql = "SELECT Nombre FROM Usuario WHERE IdEmpleado = ".$IdEmpleado;
        
        $result = mysqli_query($conn, $sql);
        
        closeDb($conn);
        
        return $result;
    }

    function getUsuariosByPuesto($Puesto){
        $conn = conectDb();
        
        $sql = "SELECT Nombre FROM Usuario WHERE Puesto = '".$Puesto."'";
        
        $result = mysqli_query($conn, $sql);
        
        closeDb($conn);
        
        return $result;
    }
?>