<?php

/**
 * Example middleware closure
 *
 * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
 * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
 * @param  callable                                 $next     Next middleware
 *
 * @return \Psr\Http\Message\ResponseInterface
 */

$app = new \Slim\App();

$app->add(function ($request, $response, $next) {
	$response->getBody()->write('Antes de todo <br><br><br><br>');
	$response = $next($request, $response);
	$response->getBody()->write('<br><br><br> Despues de todo!');

	return $response;
});

$app->get('/', function ($request, $response, $args) {
	$response->getBody()->write('<br><p>¿Cómo puede implementarse un entorno con servicios web disponibles aún cuando falle un servidor?</p><br><p>Utilizando otros servidores conectados al servicio Web que puedan obtener la información que regresaba el servidor que tiene fallas, esto se puede lograr si hay una manera generalizada de transferir información entre servidores.</p>');

	return $response;
});

$app->run();