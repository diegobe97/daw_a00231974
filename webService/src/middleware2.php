<?php

/**
 * Example middleware closure
 *
 * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
 * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
 * @param  callable                                 $next     Next middleware
 *
 * @return \Psr\Http\Message\ResponseInterface
 */

$app = new \Slim\App();

$app->add(function ($request, $response, $next) {
	$response->getBody()->write('Antes de todo <br><br><br><br>');
	$response = $next($request, $response);
	$response->getBody()->write('<br><br><br> Despues de todo!');

	return $response;
});

$app->get('/', function ($request, $response, $args) {
	$response->getBody()->write('<br><br><p>¿A qué se refiere la descentralización de servicios web?</p><br><p>A buscar la manera de reemplazar los protocolos, servicios y maneras de distribuir la información entre servicios Web y tratar de distribuirle en una manera general y evitar dependencias con otros servicios.</p>');

	return $response;
});

$app->run();