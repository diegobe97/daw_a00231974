function verificarContra() {
    let pass = document.getElementById("contraseña");
    let intento = document.getElementById("intento");
    let div = document.getElementById("validacion");

    let resultado = "<br><p>Contraseña incorrecta.</p>";
    document.getElementById("verificar").style.color = "red";
    div.style.color = "red";

    if (pass.value === intento.value) {
        resultado = "<br><p>WUJUU!! Contraseña correcta dude!! Habeis hackeado el sistema.</p>";
        document.getElementById("verificar").style.color = "green";
        div.style.color = "green";
    }

    div.innerHTML = resultado;
    setTimeout(function () {
        alert("Sabías que... En este mismo sitio, aparte de verificar contraseñas y comprar cosas, también puedes generar números aleatorios.");
    }, 5000);
}

function costo(precio, producto) {
    let cantidad = document.getElementById("cantidad" + producto).value;

    document.getElementById("prod" + producto).innerHTML = parseFloat(cantidad) * parseFloat(precio) * 84 / 100;
    document.getElementById("iva" + producto).innerHTML = parseFloat(cantidad) * parseFloat(precio) * 16 / 100;
    document.getElementById("comision" + producto).innerHTML = parseFloat(cantidad) / 100;
    document.getElementById("total" + producto).innerHTML = parseFloat(cantidad) * parseFloat(precio) + parseFloat(cantidad) / 100;
}

function generarAleatorios() {
    let valores = "<br><p>";

    for (let i = 1; i <= document.getElementById("cantidadRandom").value; i++) {
        if (i % 15 == 0) {
            valores += "</p><p>"
        }
        valores += Math.floor(Math.random() * 1000) + " | ";
    }

    valores += "</p>"
    document.getElementById("numeros").innerHTML = valores;
}

function cambiarEstilo(estilo) {
    document.getElementById("Lab5").style.fontStyle = estilo;
}

function ayudaAleatorios() {
    let parrafo = document.getElementById("ayudaAleat");
    if (parrafo.innerHTML == "") {
        parrafo.innerHTML = "Primero necesitas especificar la cantidad de números aleatorios que deseas generar,para esto debes de escribir la cantidad en el cuadro en blanco a la ziquierda del botón llamado Generar ó llegar al número deseado con las flechas que aparecen en él al pasar el cursor por encima de su área. Finalmente, solo necesitas apretar el botón llamado Generar, y aparecerán los números aleatorios abajo.";
    } else {
        parrafo.innerHTML = "";
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

document.getElementById("verificar").onclick = verificarContra;

document.getElementById("cantidadOreo").onchange = function () {
    costo(14.99, "Oreo")
};
document.getElementById("cantidadJSFD").onchange = function () {
    costo(299.99, "JSFD")
};
document.getElementById("cantidadTIC").onchange = function () {
    costo(6799.99, "TIC")
};

document.getElementById("generar").onclick = generarAleatorios;

document.getElementById("Lab5").onmouseover = function () {
    cambiarEstilo("oblique");
}
document.getElementById("Lab5").onmouseout = function () {
    cambiarEstilo("normal");
}


document.getElementById("botonAyudaAleat").onclick = ayudaAleatorios;

document.getElementById("caca1").ondrop = function () {
    drop(event)
};
document.getElementById("caca1").ondragover = function () {
    allowDrop(event)
};
document.getElementById("caca2").ondrop = function () {
    drop(event)
};
document.getElementById("caca2").ondragover = function () {
    allowDrop(event)
};
document.getElementById("caca").ondragstart = function () {
    drag(event)
};
