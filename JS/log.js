function onSignIn(googleUser) {
      var profile = googleUser.getBasicProfile();
      console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
      console.log('Name: ' + profile.getName());
      console.log('Image URL: ' + profile.getImageUrl());
      console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    
      window.location.href = 'http://localhost/daw_a00231974/index.html';
      // The ID token you need to pass to your backend:
      /*var id_token = googleUser.getAuthResponse().id_token;
      postAJAX('/server/sign-in', {id_token: id_token})
      .then(function(user) {
            // The user is now signed in on the server too
            // and the user should now have a session cookie
            // for the whole site. 
            
      })*/
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}