<!DOCTYPE html>
<html lang="es">

<head>
    <title>Lab 9</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../CSS/minstyle.css">
</head>

<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="../index.html">Diego Be</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Laboratorios<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="../HTML/HTML5.html">HTML5</a></li>
                            <li><a href="../HTML/Lab%201.html">Lab 1 - FAQ</a></li>
                            <li><a href="../HTML/Lab%202.html">Lab 2 - FAQ</a></li>
                            <li><a href="../HTML/Lab%204.html">JS Lab 4</a></li>
                            <li><a href="../HTML/Lab%205.html">Lab 5 & 6</a></li>
                            <li><a href="../PHP/Lab%209.php">PHP Lab 9</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Bases de Datos<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="../BD/Investigacion1.html">Investigación 1</a></li>
                            <li><a href="../BD/Cuestionario%201.html">Cuestionario 1</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </nav>

    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-sm-2 sidenav">
                <p><a href="#Lab9">Lab 9</a></p>
                <p><a href="#prom">Promedio</a></p>
                <p><a href="#mediana">Mediana</a></p>
                <p><a href="#sorter">Sorter</a></p>
                <p><a href="#tabla">Tabla Potencias</a></p>
            </div>
            <div class="col-sm-10 text-left">
                <h1 id="Lab9" class="stylish">Lab 9</h1>
                <hr>
                <h4 id="prom">Promedio</h4>
                <?php
                    function promedio($arreglo) {
                        $ans = "<p>El promedio de los valores: ";
                        
                        $acum = 0;
                        $cant = count($arreglo);
                        
                        for ($i = 0; $i < $cant; $i++) {
                            $ans .= "".$arreglo[$i].", ";
                            $acum = $acum + $arreglo[$i];
                        }
                        
                        $total = $acum / $cant;
                        $ans .= "es: <strong>".$total."</strong>.</p>";

                        return $ans;
                    }
                
                    $ans = promedio(array(100,90,100,100,95,72,45,100,76,38));
                    $ans .= promedio(array(41,42,43,44,45,46,47,81,82,83,84,91,92,94,29,2));
                                     
                    echo $ans;
                ?>
                <hr>
                <h4 id="mediana">Mediana</h4>
                <?php
                    function mediana($arreglo) {
                        $ans = "<p>La mediana de los valores: ";
                        $cant = count($arreglo);
                        
                        for ($i = 0; $i < $cant; $i++) {
                            $ans .= "".$arreglo[$i].", ";
                        }
                        
                        sort($arreglo);
                        
                        $mid = floor(($cant-1)/2);
                        $res = 0;
                        
                        if ($cant % 2){
                            $res = $arreglo[$mid];
                        }else{
                            $res = ($arreglo[$mid] + $arreglo[$mid + 1]) / 2;
                        }
                        
                        $ans .= "es: <strong>".$res."</strong>.</p>";
                       
                        return $ans;
                    }
                
                    $ans = mediana(array(100,90,100,100,95,72,45,100,76,38));
                    $ans .= mediana(array(41,42,43,44,45,46,47,81,82,83,84,91,92,94,29,2));
                                     
                    echo $ans;
                ?>
                <hr>
                <h4 id="sorter">Sorter</h4>
                <?php
                    function sorter($arreglo){
                        $ans = "<ul><li>Valores del arreglo:</li>";
                        $cant = count($arreglo);
                        
                        for ($i = 0; $i < $cant; $i++) {
                            $ans .= "<li>".$arreglo[$i]."</li>";
                        }
                        
                        $ans .= "<li>".promedio($arreglo)."</li>";
                        $ans .= "<li>".mediana($arreglo)."</li>";
                        
                        sort($arreglo);
                        $ans .= "<li>Arreglo ordenado de menor a mayor:";
                        
                        for ($i = 0; $i < $cant; $i++) {
                            if($i < ($cant - 1)){
                                $ans .= " ".$arreglo[$i].",";
                            }else{
                                $ans .= " ".$arreglo[$i]."</li>";
                            }
                        }
                        
                        $ans .= "<li>Arreglo ordenado de mayor a menor:";
                        
                        for ($i = $cant - 1; $i >= 0; $i--) {
                            if($i > 0){
                                $ans .= " ".$arreglo[$i].",";
                            }else{
                                $ans .= " ".$arreglo[$i]."</li>";
                            }
                        }
                        
                        $ans .= "</ul>";
                        
                        return $ans;
                    }
                
                    $ans = sorter(array(100,90,100,100,95,72,45,100,76,38));
                    $ans .= sorter(array(41,42,43,44,45,46,47,81,82,83,84,91,92,94,29,2));
                                     
                    echo $ans;
                ?>
                <hr>
                <h4 id="tabla">Tabla Potencias</h4>
                <?php
                    function tablaPotencias($n) {
                        $ans = '<table class="table table-hover">';
                        $ans .= "<thead><tr><th>Numero</th><th>Cuadrado</th><th>Cubo</th></tr></thead><tbody>";

                        for ($i = 1; $i <= $n; $i++) {
                            $ans .= "<tr><td>$i</td><td>".$i*$i."</td><td>".$i*$i*$i."</td></tr>";
                        }

                        $ans .= "</tbody></table>";

                        return $ans;
                    }

                    $ans = tablaPotencias(25);
                    $ans .= tablaPotencias(15);

                    echo $ans;
                ?>
                <hr>
                <h4 id="flip">Integer Flip</h4>
                <?php
                    function flip($n) {
                        $ans = '<table class="table table-hover">';
                        $ans .= "<thead><tr><th>Paso</th><th>Numero</th><th>Flip</th></tr></thead><tbody>";
                        
                        $i = 0;
                        $res = 0;
                        
                        $ans .= "<tr><td>".$i."</td><td>".$n."</td><td>".$res."</td></tr>";

                        do {
                            $res = $res*10 + $n%10;
                            $n = floor($n/10);
                            $i = $i + 1;
                            $ans .= "<tr><td>".$i."</td><td>".$n."</td><td>".$res."</td></tr>";
                        } while ($n > 0);

                        $ans .= "</tbody></table>";

                        return $ans;
                    }

                    $ans = flip(12345);
                    $ans .= flip(837465192);

                    echo $ans;
                ?>
                <hr>
                <h3 id="FAQ5">FAQ Lab 9</h3>
                <h4>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h4>
                <p>Muestra gran cantidad de información sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP, información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de configuración locales y generales, cabeceras HTTP y licencia de PHP.</p>
                <ul>
                    <li><strong>System</strong>: En el podemos ver la descripción del sistema en el cual se está ejecutando el archivo PHP. Me llamó la atención que al ejecutarlo y analizarlo, en este apartado se ve el sistema operativo y el nombre de la laptop en la que hice este laboratorio.</li>
                    <li><strong>PHP Credits</strong>: En él podemos ver el nombre de los autores que contribuyeron al desarrollo de PHP. Me llamó la atención la forma en la que dividen las contribuciones y que le dan crédito a cada una de las personas que ayudó en el desarrollo de cada parte y en qué parte ayudaron. Me parece algo muy ético y considero que es una gran acción para que todos los usuarios sepan de las personas detrás de PHP.</li>
                    <li><strong>Envirenment</strong>: En este apartado podemos ver más detalladamente las especificaciones del sistema en el que se ejecutó. Me llamó la atención que aquí podemos obtener bastante información proveniente del equipo en el que se corrió el archivo, pude ver información relacionada al procesador con el que uenta mi laptop.</li>
                </ul>
                <hr>
                <h4>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h4>
                <p>Mejorar la seguridad para que no sea tan sencillo de acceder a la información dentro del servidor y evitar modificaciones o pérdidas de datos realizadas por personas ajenas al sistema.</p>
                <hr>
                <h4>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h4>
                <p>Cuando un usuario solicita una página Php, el motor ejecuta el código que está en esta página. Durante la ejecucióm, el código da unas informaciones en formato html. Finalmente se envía el archivo (completamente en formato html) al usuario.</p>
                <p>Una definición más sencilla es que el navegador lo que hace es preguntarle ciertas cosas al servidor al momento de llegar a la sección que incluye el código PHP. La respuesta, por parte del servidor, se dá en formato HTML.</p>
                <hr>
                <br>
            </div>
        </div>
    </div>
    <footer class="container-fluid text-center">
        <h5><strong>Sitio desarrollado con el editor HTML <a href= "http://brackets.io/">Brackets</a></strong></h5>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../JS/Lab5.js"></script>
</body>

</html>
