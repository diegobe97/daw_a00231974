function tabla() {
    let n = window.prompt("Dame el número al que quieres llegar: ");

    let tabla = "<table><tbody>";
    
    tabla += "<tr><td>" + "Número" + "</td><td>" + "Cuadrado" + "</td><td>" + "Cubo" + "</td></tr>";

    for (let i=1; i < n; i++) {
        tabla += "<tr>";
        tabla += "<td>"+ i +"</td>";
        tabla += "<td>"+ Math.pow(i, 2) +"</td>";
        tabla += "<td>"+ Math.pow(i, 3) +"</td>";
        tabla += "</tr>";
    }
    tabla += "</tbody></table>";
    
    let div = document.getElementById("ejercicio1");
    div.innerHTML = tabla;
}

function aleatorio() {
    let n = Math.floor(Math.random() * 100);
    let m = Math.floor(Math.random() * 100);
    
    let t1 = new Date().getTime();
    let ans = window.prompt("Dame el total de la suma de " + n + "+" + m + ": ");
    let t2 = new Date().getTime();
    
    let respuesta = "<p>Respuesta incorrecta, tu tiempo fue de ";
    
    if(ans == (n + m)){
        respuesta = "<p>Respuesta correcta, tu tiempo fue de ";
    }
    
    respuesta += (t2-t1)/1000 + " segundos</p>";
    
    let div = document.getElementById("ejercicio2");
    div.innerHTML = respuesta;
}

function resCont(arr){
    let neg = 0, pos = 0, zeros = 0;
    
    for (let i=1; i<arr.length; i++) {
        if(arr[i] > 0) {
            pos++;
        }else if (arr[i] < 0) {
            neg++;
        }else {
            zeros++;
        }
    }
    
    return "<p>Tienes " + pos + " números positivos, " + zeros + " ceros y " + neg + " números negativos.</p>";
}

function contador() {
    let n = window.prompt("¿Cuántos números deseas analizar?");
    let array = new Array(n);
    
    for (let i=1; i<=n; i++) {
        array[i] = window.prompt("Dame un número, llevas " + (i-1) + " de " + n + ": ");
    }
    
    let div = document.getElementById("ejercicio3");
    div.innerHTML = resCont(array);
}

function resProm(arr, n, m){
    let respuesta = "<p>Los promedios son:</p>";
    for (let i=1; i<=n; i++) {
        let acum = parseInt(0);
        for(let j=1; j<=m; j++){
            acum += parseInt(arr[i][j]);
        }
        respuesta += "<p>" + parseInt(acum)/parseInt(m) + " para la fila " + i + ".</p>";
    }
    return respuesta;
}

function promedios() {
    let n = window.prompt("Cantidad de filas de la matríz: ");
    let m = window.prompt("Cantidad de columnas de la matríz: ");
    let array = new Array(n);
    let nums = 0;
    
    for (let i=1; i<=n; i++) {
        array[i] = new Array(m);
        for(let j=1; j<=m; j++) {
            array[i][j] = window.prompt("Dame el número de la fila " + i + " en la columna " + j + ", llevas " + nums + " de " + m*n + ":");
            nums++;
        }
    }

    let div = document.getElementById("ejercicio4");
    div.innerHTML = resProm(array, n, m);
}

function inverso() {
    let x = parseInt(window.prompt("Dame el número a invertir:"));
    let acum = parseInt(0);
    do{
        acum = parseInt(acum)*10 + parseInt(x)%10;
        x = parseInt(x)/10;
    }
    while (x > 0);
    
    let respuesta = "<p>" + acum/10 + "</p>";
    let div = document.getElementById("ejercicio5");
    div.innerHTML = respuesta;
}

function mayus() {
    let x = window.prompt("Dame la frase que quieres pasar a maypusculas:");
    let respuesta = "<p>" + x.toUpperCase() + "</p>";
    
    
    let div = document.getElementById("ejercicio6");
    div.innerHTML = respuesta;
}